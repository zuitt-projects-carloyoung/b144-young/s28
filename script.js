console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(result => {
	console.log(result)
})

// fetch('https://jsonplaceholder.typicode.com/todos?userId=1')
// .then(res => res.json())
// .then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json()) 
.then(data => {
	console.log(data) 
})

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'new to-do',
		userId: 2
	})
})
.then(res => res.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
    	title: 'New',
        description: 'update',
        status: 'pending',
        dateCompleted: 'Jan. 5',
        userID: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        status: 'on-going'
    })
})
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/2', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
   	    status: 'complete',
        dateCompleted: 'Jan. 22',
    })
})

.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'DELETE'
})
.then(res=> res.json())
.then(data=>console.log(data))

